% Implementação de Programa para *resolver* o Jogo-da-velha
% João Rabello Alvim e João Pedro Pagnan

# **Resumo**

**Neste trabalho apresentaremos brevemente um sistema baseado em teoria de jogos (por meio do algoritmo dado pelo teorema *minimax*) para resolução[^1] do jogo da velha. Será apresentado o raciocínio usado para o desenvolvimento do programa final bem como suas estruturas de dados, algoritmos e códigos propriamente dito.**[^!]


[^1]: Por resolver um jogo, queremos dizer encontrar uma forma sistemática de computar a árvore de jogos completa. A expressão "jogar perfeitamente" também adquire este mesmo sentido, em um *jogo perfeito* o jogador pode apenas ganhar ou empatar.

[^!]:\textcolor{red}{Importante}: Recomenda-se fortemente além da leitura desse breve relatório a leitura do código fonte do projeto. Sabemos que muitas vezes a leitura de um programa pode ser chata e desmotivadora, no entanto acreditamos que um programa seja muito mais do que apenas *software*, quando em sua forma de **código fonte** estamos em contato com um **texto**, e portanto, ficamos muito mais próximos das ideias e da forma de pensar do autor. O Código fonte é um gênero textual com suas características e particularidades, por meio de *comentários* somos capazes de transmitir intenções, problemas e até piadas! Nos divertimos muito escrevendo esse programa e esperamos que você, professor Romis, também se divirta lendo-o!

# Introdução

Para a disciplina IA006 estivemos nos últimos meses inicialmente compreendendo alguns aspectos mais gerais sobre o jogo de xadrez e sobre o tópico matemático de teoria dos jogos. Neste momento da disciplina, estudamos o teorema *minimax*[^2] como uma solução inicial para o problema do xadrez computacional. Como uma forma de melhor internalizar o tema e os tópicos associados à teoria de jogos de soma zero, sendo estes: a busca em árvore, representação lógico/numérica de jogos e funções de avaliação, foi solicitada a elaboração de um programa que jogue *perfeitamente* o jogo da velha.

[^2]: Teorema desenvolvido por Von Neumann para a resolução de jogos de soma zero. Mais informações neste [\textcolor{blue}{link}](https://en.wikipedia.org/wiki/Minimax_theorem)

Para este fim, implementamos na linguagem `Python` um programa contendo as subrotinas necessárias para as etapas enumeradas acima. Este programa está melhor explicado nas próximas seções, no entanto, para entender o o funcionamento do programa em si, recomenda-se fortemente que seja lido o código fonte.

# Arquitetura do programa

Para falar da arquitetura do programa, primeiramente falaremos da interface de nosso programa com o mundo. Para executá-lo pode-se, a partir de alguma interface de texto, escrever a seguinte linha de comando:

```bash
	$ python3 jogo_da_velha.py
```

Nesse momento você verá o seguinte texto:

> Olá, sou o Deus do jogo da velha, você ousa me desafiar?
> Estou invocando o poder de todos os jogos possíveis!
> Isso pode demorar um pouquinho...

O que será que está acontecendo por debaixo dos panos? O que o nosso programinha petulante está tramando por trás da sua *interface*[^3]? Nesse momento é oportuno introduzirmos o primeiro tópico da nossa arquitetura de *software*:

[^3]: A quem interessar, segue o link para o [\textcolor{blue}{repositório}](https://gitlab.com/jalvim/jogo-da-veia/)

## Construção da árvore de jogos

Enquanto a nossa interface está provocando de forma herética o desavisado jogador, por debaixo dos panos, encontra-se o seguinte segmento de código:

```python
	print('Olá, sou o Deus do jogo da velha, você ousa me desafiar?')
	print('Estou invocando o poder de todos os jogos possíveis!')
	print('Isso pode demorar um pouquinho...')

	root, leaves = build_tree()
```

Neste segmento podemos identificar a função `build_tree()`. Nela está contida a maior parte da complexidade computacional do nosso robozinho (se eu fosse chutar, diria que entre 50% e 60%). Para compreender como a árvore de possíveis jogos é construída, apresentamos um subtópico de *representação do jogo*:

- **O ringue**: O subespaço das matrizes reais $3 \times 3$ $\mathcal{C}$ com entradas inteiras tais que seus elementos estejam no conjunto $\{0, 1, 2\}$, ou seja, em pleno matematiquês:
$$
	\mathcal{C} = \big\{C \in \mathbb{Z}^{3 \times 3} \ | \ c_{i, j} \in [0, 1, 2] \big\}
$$
- **Os lutadores**: Sabendo que nossa representação de tabuleiro passa é dada por uma matriz $C \in \mathcal{C}$[^4] com entradas pertencentes ao conjunto $\{0, 1, 2\}$, para darmos um sentido para essa matriz, fazemos a seguinte associação simbólica:
	- *x* está para 1
	- *o* está para 2
	- *\` '* (casa vazia) está para 0

[^4]: Percebe-se que o subconjunto de configurações possíveis de jogos da velha tende a ser ainda menor do que o enunciado, visto que, uma vez atingida alguma *configuração de vitória*, qualquer matriz derivada $D \in \mathbb{Z}^{3 \times 3}$ torna-se **inválida**, e portanto $D \notin \mathcal{C}$

Agora que nos é sabido como representar o espaço de possíveis jogos da velha (também conhecido por nós como *espaço de configurações*), o conjunto $\mathcal{C}$, para realizarmos nosso objetivo final -desenvolver um programa que jogue perfeitamente o jogo da velha- percebe-se que seria bastante oportuno uma forma de avaliar o quanto uma configuração $C_i \in \mathcal{C}$ está próxima de uma vitória ou derrota. Este fim pode ser alcançado se usada a função `eval_config`, também presente no escopo de nosso programa.

Uma vez que o conjunto de possíveis configurações de jogos da velha é um espaço composto por  $255'168$ matrizes (ou seja, $|\mathcal{C}| = 255'168$), podemos categorizar, pela estrutura em árvore três escores possíveis:

1. Vitória da peça *x* $\implies$ $10$ pontos
1. Vitória da peça *o* $\implies$ $-10$ pontos
1. Empate $\implies$ $0$ pontos

Para elencarmos qual escore associamos a cada configuração observemos o corpo da função `eval_config`:

```python
eval_config(T):
    # Declara variável de escore 
    score = 0

    # Itera por índice (avaliação de linhas e colunas)
    for ii in range(3):
        # Verifica se há fechamento de coluna
        if T[ii][0] != 0:
	    if T[ii][0] == T[ii][1] and T[ii][1] == T[ii][2]:
                expo  = T[ii][0]
                score = 10 * np.power(-1, expo)
                return score

        # Verifica se há fechamento de linha
        if T[0][ii] != 0:
            if T[0][ii] == T[1][ii] and T[1][ii] == T[2][ii]:
                expo  = T[0][ii]
                score = 10 * np.power(-1, expo)
                return score

    # Verifica vitória pelo fechamento da diag. principal
    if T[0][0] != 0:
        if T[0][0] == T[1][1] and T[1][1] == T[2][2]:
            expo  = T[0][0]
            score = 10 * np.power(-1, expo)
            return score

    # Verifica vitória pelo fechamento da diag. secundária
    if T[2][0] != 0:
        if T[2][0] == T[1][1] and T[1][1] == T[0][2]:
            expo  = T[0][2]
            score = 10 * np.power(-1, expo)
            return score

    return score
```

Percebe-se que, inicialmente atribui-se $0$ como o escore da configuração e avalia-se a partir das posições do tabuleiro se foi alcançada ou não a condição de vitória (preenchimento total de linha, coluna ou digonal por alguma das peças). Caso nenhuma condição seja atingida, o escore permanece como $0$, situação conhecida por **velha**.

Para a construção da árvore de jogos em si, a função `eval_config` tem um papel importante para estabelecer uma condição de parada. Como as configurações de vitória (ou derrota) são *terminais*, ou seja, folhas da nossa árvore, é importante interromper a recursão[^5] no momento em que tais configurações são atingidas.

[^5]: Importante observar que, no nosso trabalho, escolhemos construir a árvore de jogos de maneira recursiva. A descrição do algoritmo em si será desenvolvida posteriormente.

Com o funcionamento adequado de uma função de avaliação agora é possível implementar uma função de construção de árvore. Agora com intenção de melhor compreender a função `build_tree`, explicaremos como a função atua de forma recursiva para a construção da árvore de configurações possíveis.

O nó raiz da árvore é o tabuleiro vazio, representado pela matriz $3 \times 3$ com todas as suas entradas nulas. A partir dessa configuração inicial, um mecanismo autônomo de detecção de vez (algo tão simples quanto `turn *= -1` ao fim de cada nível da recursão) e uma forma de identificar as casas vazias no tabuleiro - algo que módulo `numpy` faz de forma primorosa! - conseguimos implementar uma função recursiva de construção de árvore de jogos.

De forma simplificada, o algoritmo de construção da árvore funciona da seguinte forma:

1. Verifica o escore da configuração avaliada
2. Em caso de escore $\pm 10$, encerra a função, em caso de escore 0, segue para 3.
3. Lista as posições vazias no tabuleiro
4. Em caso de lista vazia, encerra a função, caso o contrário, segue para 5.
5. Itera por cada configuração filha preenchendo o espaço vazio pela peça associada ao turno da rodada e realiza a chamada recursiva sobre cada configuração (voltando ao passo 1.) invertendo o turno da rodada.

A sistematização enunciada na listagem acima assume sua forma final a partir da seguinte função na linguagem `Python`:

```python
def tree_rec (node, player):
    # configura variáveis de manejo de jogador
    play       =  1  if player == 'x' else  2
    player_nxt = 'x' if player == 'o' else 'o'

    # Adquire índices de posições vazias na configuração
    z_idx, z_idy = np.nonzero(node.config == 0)

    # Verifica se o tab. está cheio, se sim, acrescenta a
    # posição como folha
    if len(z_idx) == 0:
        leaves.append(node)

    # Verifica se a posição é ganha ou perdida, se sim,
    # acrescenta o nó na tabela de folhas
    if node.score == 10 or node.score == -10:
        leaves.append(node)
        return

    nonlocal id

    # Cria vetor de matrizes temporárias
    Tt = [None] * len(z_idx)

    # Itera sobre cada casa vazia do tabuleiro e cria
    # um nó filho associado ao nó pai
    for ii, (zx, zy) in enumerate(zip(z_idx, z_idy)):
        # Constroi matriz da posição de interesse
        Tt[ii]         = deepcopy(node.config)
        Tt[ii][zx, zy] = int(play)

        # Atualiza o índice de id global
        id = id + 1

        # Cria nós temporários para o manejo da
        tmp = AnyNode(
    	    id        = str(id),
    	    parent    = node,
    	    config    = Tt[ii],
    	    turn      = player,
    	    score     = eval_config(Tt[ii]),
    	    opt_idx   = 0,
    	    color     = 0
        )

    # Itera por nós filhos do sistema em questão e realiza
    # a chamada recusrsiva de construção.
    for n in node.children:
        tree_rec(n, player_nxt)

    return
```

Para nos auxiliar no desenvolvimento da atividade, usamos o módulo `anytree`. Uma biblioteca especializada com uma implementação já bastante otimizada para construção e manejo de árvores. A partir dela, algoritmos de ordenamento e ligação dos nós são problemas resolvidos.

Nessa etapa, percebe-se que a computação associada à construção da árvore é bastante custosa. Para dar uma enganada no interlocutor, optamos por dar uma personalidade assoberbada para o nosso robozinho. Enquanto ele conta vantagem, o programa constrói a árvore de jogos. Como este processo é excessivamente custoso para uma tarefa tão boba quanto uma partida de jogo da velha, optamos por armazenar a árvore computada na primeira jogada. Este processo é viabilizado pela biblioteca `pickle`. Dessa forma, depois de jogada a primeira partida, é reduzido o tempo total de processamento, visto que não é mais necessária a computação da árvore.

Como o jogo da velha apresenta um número factível de possíveis jogos, percebe-se que, diferentemente de um jogo de xadrez ou damas, não é necessário o desenvolvimento de uma função de avaliação. No entanto, para que o pleno funcionamento do sistema em si, é necessário ainda implementar a lógica do algoritmo *minimax*[^6]. Portanto, avançamos para o próximo tópico do trabalho.

[^6]: Se no início do texto falamos que cerca de 50\% do custo computacional do trabalho está na etapa de construção da árvore, o restante está na execução do *minimax*.

## Algoritmo *minimax*

O algoritmo *minimax*, conforme abordado em sala de aula, consiste na estratégia ótima para jogos de soma zero[^7]. Em linhas gerais, o método consiste em, dada a árvore avaliada, a retro-propagação dos pesos das folhas (nós terminais da estrutura) da árvore de jogos a partir da maximização ou minimização destes a depender do turno da rodada.

[^7]: Jogos em que não há possibilidade de cooperação entre jogadores.

Podemos pensar da seguinte forma: quando o sistema joga (escolhe-se $C_i \in \mathcal{C}$) é selecionada a configuração que deixe para o oponente **o pior melhor caso**. Isso significa limitar o movimento do adversário para um subconjunto $\Omega = \{C_i\}_{i=1}^N \subset \mathcal{C}$ que o máximo escore $f: C_i \mapsto \alpha\ | \ C_i \in \Omega, \ \alpha \in \mathbb{R}$[^8] seja o mínimo dentre todas as configurações *filhas* daquela apresentada no turno.

[^8]: Permitindo um certo abuso de notação, a aplicação $f: \Omega \rightarrow \mathbb{R}$ apresentada representa nossa função de avaliação, ou seja, é equivalente a rotina `eval_config` anteriormente apresentada.

Para realizar o algoritmo *minimax*, optamos por, primeiramente, construir toda a árvore de jogos para só depois realizar a retro-propagação dos escores pela estrutura. Dessa forma, a rotina implementada tem uma abordagem *bottom-up* do problema, começando a propagação dos escores das camadas mais externas (folhas) até a mais interna, a raiz, representada pela *configuração inicial*

Sabendo que a primeira vez é sempre do adversário[^9], podemos avaliar qual operação (maximização ou minimização) deve ser feita em cada turno para a propagação do escore. associamos, portanto, a seguinte atribuição de operação por turno:

- Vez do sistema (turno = *x*) - **maximização**: seleciona-se a configuração com maior escore dentre as filhas
- Vez do adversário (turno = *o*) - **minimização**: é escolhida a configuração filha com menor escore

[^9]: Minha mãe me ensinou que o contrário é falta de educação e não irei contraria-la...

A implementação deste algoritmo de retro-propagação está implementado no programa por meio da seguinte rotina[^10]:

```python
def backprop_tree (leaves, root):
    def backprop (node):
        # Verifica se nó é raiz ou já foi visitado ante-
        # riormente e encerra o programa.
        if node.is_root or node.color == 1:
            return 

        # Indica no nó que ele foi visitado
        node.color = 1

        # Resgata informação de turno
        turn = node.turn

        # Verifica se nó tem filhos (determinar peso)
        if not node.is_leaf:
            # Cria variáveis temporárias
            num_child  = len(node.children)
            tmp_scores = np.zeros(num_child)

            # Adquire escore de todas as vars filhas
            for ii, child in enumerate(node.children):
                tmp_scores[ii] = child.score

            # Verifica se o turno é p/ 'x' ou 'o'
            if turn == 'o':
                # Guardando o melhor escore, para o caso
                # da nossa jogada
                node.score   = np.min(tmp_scores) + 1
                node.opt_idx = np.argmin(tmp_scores)
            else:
                # Guardando o pior escore, para o caso
                # de jogada do oponente.
                node.score   = np.max(tmp_scores) - 1
                node.opt_idx = np.argmax(tmp_scores)

        # Encerra a função interna
        return

    # Itera por camadas em cada nível em ordem reversa
    # (das folhas até a raiz)
    node_lst = list(LevelOrderIter(root))
    for node in reversed(node_lst):
        # Verifica se nó é raiz e interrompe o laço
        if node.is_root or node.color == 1:
            continue

        # Calcula o peso do nó por retropropagação
        backprop(node)

    # Encerra a função
    return
```

[^10]: Achou estranho uma função dentro da outra? Realmente é, mas tem uma explicação. Quer saber qual? Leia o código fonte!

É sabido que a etapa de retro-propagação do algoritmo *minimax* é também por si só uma etapa bastante complexa em termos computacionais e, consequentemente, demorada para o adversário... Para aliviar o tédio da espera, continuamos com a nossa lorotinha imprimindo o texto:

>    ... Falta pouco! Agora só preciso digerir
>    Isso pode demorar mais um pouquinho...

## Interface de jogo

Como é um jogo bastante simples, optamos por manter a interface igualmente simples. Foi implementada uma \textcolor{red}{TUI} (do inglês *terminal user interface*) baseada em caracteres UTF-8 que indica as casas vazias a partir de um índice[^11]. Na sua forma de texto pleno, podemos acompanhar as duas primeiras jogadas do adversário (jogado *o*) na próxima página:

\pagebreak

```
Olá, sou o Deus do jogo da velha, você ousa me desafiar?
Estou invocando o poder de todos os jogos possíveis!
Isso pode demorar um pouquinho...
... Falta pouco! Agora só preciso digerir
Isso pode demorar mais um pouquinho...
Jogue comigo se for capaz!
Como minha humildade é magnânima,
deixarei você começar

Você usará a peça: "o"
Achas que tens o que é necessário para me desafiar?
Digite "s" para acietar o desafio: s
boa sorte...
... vc irá precisar!
 | | 	 0|1|2
—+—+—	 —+—+—
 | | 	 3|4|5
—+—+—	 —+—+—
 | | 	 6|7|8
insira um num: 4
 | | 
—+—+—
 |o| 
—+—+—
 | | 
x| | 	  |0|1
—+—+—	 —+—+—
 |o| 	 2| |3
—+—+—	 —+—+—
 | | 	 4|5|6
insira um num: 4
x| | 
—+—+—
 |o| 
—+—+—
o| | 
x| |x	  |0| 
—+—+—	 —+—+—
 |o| 	 1| |2
—+—+—	 —+—+—
o| | 	  |3|4
insira um num:
```

[^11]: Índice que convenientemente é mapeado no índice da configuração filha equivalente à posição em que se deseja ocupar.

\pagebreak

A partir da seleção automática de índices, o jogo avança até o seu desfecho (que até os últimos testes, sempre acabam em empate ou derrota). Ainda há mais diálogos com nosso personagem espirituoso e petulante, no entanto, não iremos estragar todas as surpresas e deixaremos que o leitor descubra tudo jogando nosso joguinho.

# Conclusões gerais

Este relatório tem como objetivo descrever o funcionamento do programa desenvolvido para a tarefa de *jogar perfeitamente* o jogo da velha. Para tanto, apresentamos os principais algoritmos, estruturas de dados e segmentos de código associado ao programa desenvolvido com este fim. Apesar do programa ser funcional (nós pelo menos nunca conseguimos ganhar do nosso personagem petulante), conhecemos as falhas do nosso sistema e precisamos enumerá-las para apontar perspectivas de melhorias gerais. Alguns desses pontos são:

- **Sistema de poda de árvore**: para evitar processamento com configurações perdidas;
- **Interface gráfica**: Implementar uma interface de usuário que seja um pouco mais intuitiva;
- **Seleção aleatória de movimentos de mesmo escore**: Não foi sistematizada uma seleção aleatória de *movimentos equivalentes* (aqueles que compartilham o mesmo escore após o processamento do *minimax*). Essa melhoria, apesar de não essencial para o funcionamento do projeto, permite uma maior dinamicidade e melhoria na jogabilidade, visto que dois jogos nunca seriam iguais.

Através das propostas de melhoria enumeradas, é possível relatar de forma honesta o processo de desenvolvimento, suas potencialidades e atuais gargalos. Esperamos conseguir, eventualmente, superar estes problemas e assim conseguir, de forma definitiva, finalizar o projeto.

A experiência de desenvolver o *sisteminha* proposto foi surpreendentemente divertida para um problema de matemática computacional! Através dela já tivemos *insights* valiosos para a elaboração de nosso projeto final[^12] e sabemos que este conhecimento adquirido será ainda de grande valia para o desenvolvimento de nossas pesquisas.

[^12]: Seria o *projeto damas* um velha-O-nator parte II?
