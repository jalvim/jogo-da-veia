#!/bin/python
# VELHA-O-NATOR 3000
# autores: jalvim & pagna

# Imports dos módulos usados ao logo do programa 
import numpy as np
from copy import deepcopy
from math import factorial
from time import sleep
from anytree import AnyNode, LevelOrderIter
from pickle import dump, load
from os.path import exists
import math as m

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# IMPORTANTE DE SE RELATAR:
#
# Comecei a fazer esse programa, confesso, que em um pri-
# meiro momento, num lapso de inocência e ingenuidade co-
# mecei este programa pensando na boba possibilidade de
# implementar uma árvore completa do jogo da velha. No
# entanto, logo quem comecei a relfletir melhor sobre o
# assunto percebi que:
#
# - Uma árvore que se inicia da posição:
#
#                | |
#               —+—+—
#                | |
#               —+—+—
#                | |
#
#   terá um total de 9! (ou seja: 362880) possíveis jo-
#   gos, dentro de vitórias, derrotas e empates. No en-
#   tanto, se estamos jogando como "Deus" (sempre com a
#   possibilidade de ganhar ou empatar), preciso de ran-
#   quear não só as "folhas", como também todos os nós
#   da árvore!!! Perceba, no total, para ter a estrutu-
#   ra para o nosso pleno usufruto seria necessário re-
#   alizar um total de:
#
#   9! + 8! + 7! + ... + 3! + 2! + 1!
#
#   Com a seguinte linha de código podemos verificar que
#   este número é:
#
#       print(sum([factorial(ii) for ii in range(1, 10)]))          # <-- PODE DESCOMENTAR SE QUISER
#
#   409113. Muito grande e muito custoso para se fazer
#   na minha calculadora de bolso...
#
# Para solucionar esta questão, pensei em reduzir o cus-
# to computacional médio do meu sisteminha através de um
# mecanismo baseado em *programação dinâmica* <fiu fiu>
#
# ^^^^^^ Deu xabú nessa ideia aqui...
#
# Acabei implementando tudo na força bruta mesmo... Mas,
# justamente pra deixar a atividade de jogar um pouco ma-
# is suportável, fiz o seguinte:
#
#       - Na primeira execução do programa, computo o di-
#         abo da árvore, daí, quando ela está pronta, sal-
#         vo a estrutura em um binário via `Pickle`. Quan- 
#         do executar de novo, não é necessário mais car-
#         regar a árvore.
#
# Para funs de comparação, percebe-se que a computação da
# estrutura da árvore demora cerca de:
#
# 16.56 segundos
#
# Para carregar a árvore, uma vez que o arquivo já existe,
# mediu-se um tempo de aproximadamente
#
# 8.15 segundos
#
# Uma redução de praticamente 50% do tempo de processamen-
# to!! Ainda sim, acredito que mais otimizações possam ser
# feitas para melhorar o tempo de espera do joguim. Até
# pq o maior peso computacional (incluindo o minimax) do
# programa está na construção da árvore de jogos.

# ----------- ESTRUTURAS DE DADOS UTILIZADAS -----------

# Busca-se neste segmento do código declarar as estrutu-
# ras de dados usadas ao longo do programa. Primeiramente
# definem-se os nós para a árvore de jogos usada para a
# implementação da estratégia minimax. Neste sentido, fo-
# ram usados os seguintes campos para a estrutura:
#
#       - node.config:    Matriz contendo a configuração atual
#                         do jogo
#       - node.parent:    Campo indicando o nó de origem para
#                         esta jogada
#       - node.score:     Campo indicando o escore associado à
#                         configuração atual
#       - node.turn:      Campo indicando jogador com a vez
#       - node.child:     Campo indicando os possíveis nós
#       - node.opt_idx:   Campo com índice de mov. ótimo (pre-
#                         enchido apenas na etapa de minimax)
#       - node.chil_conf: Campo lista de conf. de nós filhos
#       - node.color:     Campo p/ verificação de passagem
#                         anterior em inspeção de árvore
#
# A matriz de representação do jogo da velha é de tama-
# nho 3x3 e possui a seguinte relação numérica:
#
#          ' ' --> 0
#           x  --> 1                    // NÓS (the compiuter) SEMPRE JOGAMOS COM 'o' E SOMOS OS 2o's A JOGAR
#           o  --> 2
#
# Essa escolha de números se deve a uma maior facilidade
# para indexação, o que fará sentido para as funções de
# vizualização de tabuleiro. dessa forma, cria-se a equi-
# valência:
#
#       1 2 0             x|o|
#                         —+—+—
#       1 1 2     -->     x|x|o
#                         —+—+—
#       2 0 1             o| |x

def eval_config (T):
    '''Função responsável por avaliar escore para um de-
       terminado campo. Essa função é feita a partir da
       definição de um escore para a configuração de in-
       teresse, podendo sera:
       10  - Vitória para "o"
       0   - Empate ou jogo corrente
       -10 - Vitória para "x"

       Inputs:
           T     --> Configuração do tabuleiro
       Outputs:
           score --> Escore dado para a configuração'''

    # Declara variável de escore 
    score = 0

    # Itera por índice (avaliação de linhas e colunas)
    for ii in range(3):
        # Verifica se há fechamento de coluna
        if T[ii][0] != 0:
            if T[ii][0] == T[ii][1] and T[ii][1] == T[ii][2]:
                expo  = T[ii][0]
                score = 10 * np.power(-1, expo)
                return score

        # Verifica se há fechamento de linha
        if T[0][ii] != 0:
            if T[0][ii] == T[1][ii] and T[1][ii] == T[2][ii]:
                expo  = T[0][ii]
                score = 10 * np.power(-1, expo)
                return score

    # Verifica vitória pelo fechamento da diag. principal
    if T[0][0] != 0:
        if T[0][0] == T[1][1] and T[1][1] == T[2][2]:
            expo  = T[0][0]
            score = 10 * np.power(-1, expo)
            return score

    # Verifica vitória pelo fechamento da diag. secundária
    if T[2][0] != 0:
        if T[2][0] == T[1][1] and T[1][1] == T[0][2]:
            expo  = T[0][2]
            score = 10 * np.power(-1, expo)
            return score

    return score

# ^^^COMENTÁRIO:
#
#       Essa função poderia ser melhor otimizada usando a nova
#       funcionalidade da linguagem Python disponível a partir
#       da versão 3.11, o famingerado *pattern-matching*. No
#       entanto eu não posso assegurar que qm for rodar esse
#       bicho vai tá na mesma versão do python q eu, né? ...
#       ... Paciência ...

def build_tree (T=np.zeros((3, 3), dtype=np.int64), player='x'):
    '''Função responsável por construir a árvore de jogos
       para o jogo da velha. A partir dessa estrutura de
       dados é que poderemos avaliar as estratégias óti-
       mas a partir da retro-proparagação dos escores de
       cada jogo.

       Inputs:
           T      --> (opcional) apresenta configuração avaliada
           player --> (opcional) flag que declara quem faz a
                      jogada em cada configuração
       Outputs:
           root   --> Nó contendo o ptr para a raiz da árvore
           leaves --> Vetor contendo todos os nós de fim de
                      jogo'''

    # Verifica se o trampo da árvore já foi feito uma vez
    if exists('./arvore.pkl'):
        with open('./arvore.pkl', 'rb') as fp:
            # Lê árvore do arquivo
            root = load(fp)

            # Recolhe as folhas em lista
            leaves = list(root.leaves)

        # Encerra e retorna a função
        return root, leaves

    # Declara nó de raiz da árvore e sua ID para ref.
    id   = 0
    root = AnyNode(
        id        = str(id),          # Determina string de id da raiz
        parent    = None,             # Por ser raiz, pai é nulo
        config    = T,                # Associa a configuração à raiz
        turn      = player,           # Indica de quem é a vez
        score     = eval_config(T),   # Associa o escore da posição
        opt_idx   = 0,                # Posição com "placeholder"
        color     = 0                 # Indicação que ngm passou aqui
    )

    # Declara próximo a jogar
    player_nxt = 'o'

    # Declara vetor de jogos finais.
    leaves = []

    # Função de criação recursiva de árvore, mais info abaixo...
    def tree_rec (node, player):
        '''Função recursiva de escopo interno (para poder acessar as
           variaáveis locais à função). Essa implementação avalia to-
           dos os possíveis movimentos, associa estes como filhos, e
           os seus respectivos escores. Em caso de vitória, derrota
           ou velha, a configuração é acrescida na lista "leaves".

           Inputs:
               parent --> Nó da configuração avaliada
               player --> Indicação do jogador da rodada'''

        # configura variáveis de manejo de jogador
        play       =  1  if player == 'x' else  2
        player_nxt = 'x' if player == 'o' else 'o'

        # Adquire índices de posições vazias na configuração
        z_idx, z_idy = np.nonzero(node.config == 0)

        # Verifica se o tab. está cheio, se sim, acrescenta a
        # posição como folha
        if len(z_idx) == 0:
            leaves.append(node)

        # Verifica se a posição é ganha ou perdida, se sim,
        # acrescenta o nó na tabela de folhas
        if node.score == 10 or node.score == -10:
            leaves.append(node)
            return

        nonlocal id

        # Cria vetor de matrizes temporárias
        Tt = [None] * len(z_idx)

        # Itera sobre cada casa vazia do tabuleiro e cria
        # um nó filho associado ao nó pai
        for ii, (zx, zy) in enumerate(zip(z_idx, z_idy)):
            # Constroi matriz da posição de interesse
            Tt[ii]         = deepcopy(node.config)
            Tt[ii][zx, zy] = int(play)

            # Atualiza o índice de id global
            id = id + 1

            # Cria nós temporários para o manejo da
            tmp = AnyNode(
                id        = str(id),
                parent    = node,
                config    = Tt[ii],
                turn      = player,
                score     = eval_config(Tt[ii]),
                opt_idx   = 0,
                color     = 0
            )

        # Itera por nós filhos do sistema em questão e realiza
        # a chamada recusrsiva de construção.
        for n in node.children:
            tree_rec(n, player_nxt)

        return

    # Constroi a árvore a partir do nó de raiz
    tree_rec(root, player_nxt)

    # Escreve a árvore com o Exporter em formato JSON
    # desse jeit, o bicho só demora uma vez só.
    if not exists('./arvore.pkl'):
        # Abre o arquivo pra poder ler o conteúdo
        # pra raiz
        with open('arvore.pkl', 'wb') as fp:
            dump(root, fp)

    return root, leaves

# --------- FUNÇÕES DE ENUMERAÇÃO DE ESTRATÉGIAS ----------

# Neste bloco estão organizadas as funções utilizadas duran-
# te o jogo em si. A avaliação das possibilidades existentes
# e a retropropagação dos escores pelas árvores parciais é
# montado a partir das funções aqui declaradas.

def backprop_tree (leaves, root):
    '''Função responsável pela implementação do algo-
       ritmo minimax em si. Este procedimento consis-
       te em calcular os escores para os nós "secun-
       dários" da árvore. Essa atribuição é feita a-
       través da MAXIMIZAÇÃO DO PIOR CASO para as jo-
       gadas feita pelo algoritmo e da MINIMIZAÇÃO DO
       MELHOR CASO, quando avaliado os turnos do opo-
       nente humano.

       Inputs:
            leaves --> Vetor de folhas da árvore
            root   --> Nó de raiz da estrutura de
                       árvore.'''

    # Assim como a função de criação da árvore, esta
    # função foi construída com o intuiro de ter uma
    # parte recursive, dessa forma, define-se o méto-
    # do:
    def backprop (node):
        '''Função interna que avalia (de forma parcial
           e iterativa) os escores de cada nó secundário
           baseado no algoritmo de minimiax - a partir do
           turno de cada configuração.

           Inputs:
                node --> Nó avaliado'''

        # Verifica se nó é raiz ou já foi visitado ante-
        # riormente e encerra o programa.
        if node.is_root or node.color == 1:
            return 

        # Indica no nó que ele foi visitado
        node.color = 1

        # Resgata informação de turno
        turn = node.turn

        # Verifica se nó tem filhos (determinar peso)
        if not node.is_leaf:
            # Cria variáveis temporárias
            num_child  = len(node.children)
            tmp_scores = np.zeros(num_child)

            # Adquire escore de todas as vars filhas
            for ii, child in enumerate(node.children):
                tmp_scores[ii] = child.score

            # Verifica se o turno é p/ 'x' ou 'o'
            if turn == 'o':
                # Guardando o melhor escore, para o caso
                # da nossa jogada
                node.score   = np.min(tmp_scores) + 1
                node.opt_idx = np.argmin(tmp_scores)
            else:
                # Guardando o pior escore, para o caso
                # de jogada do oponente.
                node.score   = np.max(tmp_scores) - 1
                node.opt_idx = np.argmax(tmp_scores)

        # Encerra a função interna
        return

    # Itera por camadas em cada nível em ordem reversa
    # (das folhas até a raiz)
    node_lst = list(LevelOrderIter(root))
    for node in reversed(node_lst):
        # Verifica se nó é raiz e interrompe o laço
        if node.is_root or node.color == 1:
            continue

        # Calcula o peso do nó por retropropagação
        backprop(node)

    # Encerra a função
    return

# ----------- FUNÇÕES DE I/O E INTERFACE DE USR -----------

def print_tab (T, player='o'):
    '''Função de impressão do tabuleiro de jogo
       da velha.

       Inputs:
            T --> Matriz 3x3 com jogo a ser disposto'''

    # Verifica qual o jogador joga o turno
    if player == 'o':
        # Declaração do vetor de representações
        rep_vec = [' ', 'x', 'o']

        # Impressão do tabuleiro linha a linha
        print(f'{rep_vec[T[0, 0]]}|{rep_vec[T[0, 1]]}|{rep_vec[T[0, 2]]}')
        print('—+—+—')
        print(f'{rep_vec[T[1, 0]]}|{rep_vec[T[1, 1]]}|{rep_vec[T[1, 2]]}')
        print('—+—+—')
        print(f'{rep_vec[T[2, 0]]}|{rep_vec[T[2, 1]]}|{rep_vec[T[2, 2]]}')
    else:
        # Declaração do vetor de representações
        rep_vec = [' ', 'x', 'o']

        # Declaração da matriz de representação
        R = np.array([['█', '█', '█'],
                      ['█', '█', '█'],
                      ['█', '█', '█']])

        # Adquire índices de casas vazias
        z_idx, z_idy = np.nonzero(T == 0)

        # Declaração de índice preliminar
        idx = 0

        # Verifica se a posição é vazia
        for ii, jj in zip(z_idx, z_idy):
            R[ii, jj] = str(idx)
            idx      += 1
            continue

        # Impressão do tabuleiro linha a linha
        print(
            f'{rep_vec[T[0, 0]]}|{rep_vec[T[0, 1]]}|{rep_vec[T[0, 2]]}\t',
            f'{R[0, 0]}|{R[0, 1]}|{R[0, 2]}'
        )
        print('—+—+—\t', '—+—+—')
        print(
            f'{rep_vec[T[1, 0]]}|{rep_vec[T[1, 1]]}|{rep_vec[T[1, 2]]}\t',
            f'{R[1, 0]}|{R[1, 1]}|{R[1, 2]}'
        )
        print('—+—+—\t', '—+—+—')
        print(
            f'{rep_vec[T[2, 0]]}|{rep_vec[T[2, 1]]}|{rep_vec[T[2, 2]]}\t',
            f'{R[2, 0]}|{R[2, 1]}|{R[2, 2]}'
        )
    

# ---------------- MÓDULO PRINCIPAL ----------------

def main (node, debug_flag=False):
    # Tempo p/ ...tan dAn DAN... acrescentar suspense
    sleep(0.5)

    # Anúnucio de regras
    print('Como minha humildade é magnânima,')
    print('deixarei você começar')
    print()
    print('Você usará a peça: "o"')

    # Lero-lero de desafiar deus e é essas
    # besteiras
    print('Achas que tens o que é necessário para', end='')
    opt = input(' me desafiar?\nDigite "s" para acietar o desafio: ')

    # Manejo de aciete de lero-lero
    if (opt != "s"):
        print('Pelo menos você reconhece o seu lugar!')
        print('Deus é imbatível!!')
        return

    # fim do lero-lero
    print('boa sorte...')
    sleep(0.5)
    print('... vc irá precisar!')

    # Laço principal de jogo
    while True:
        # Adquire num. de nós filhos
        num_fil = len(node.children)

        # Valor inicial do índice de avaliação
        idx = 100
        pre = 'a'

        # Se o modo de debug estiver ligado, abre as en-
        # tranhas do código pra todo mundo ver a baixa-
        # ria que tá rolando no backstage....
        if debug_flag is True:
            # Ups... Fui pego no pulo! KkkKkKkKkkkkKK
            turn = 'o' if node.turn == 'x' else 'x'
            print(f'Escore da configuração: {node.score}')
            print('Escores das conf\'s filhas:')
            print(f'Jogada de: {turn}')
            print('[', end='')
            for ii, child in enumerate(node.children):
                print(child.score, ' ', end='')

            print(']')
            print(f'Índice de melhor escolha: {child.opt_idx}')

        # Imprime configuração com interface rudimentar
        print_tab(node.config, node.turn)
        if node.is_leaf:
            break

        # Maneja o turno com entrada de usuário
        if node.turn == 'x':
            pre = input('insira um num: ')
            if pre.isnumeric():
                idx = int(pre)

            # Firulinha pro programa n crashar com entrada errada
            while idx not in range(num_fil) or not pre.isnumeric():
                print(f'Eu falei, número, e de 0 a {num_fil - 1},', end='')
                pre = input(' insolente: ')
                if pre.isnumeric():
                    idx = int(pre)

        # Apenas atualiza com peso do minimax na
        # rodada atual
        else:
            idx = node.opt_idx
            # Mais ...tAn DaN !!DAN!!... suspense ainda!
            sleep(0.5)

        node = node.children[idx]

    # Mensagens de fim de jogo:
    if node.score > 0:
        # Se vc chegou até aqui, parabéns, vc encontrou um bug!
        # Peço pra vc por favor entrar em contato com o e-mail
        #
        # j218572@dac.unicamp.br
        #
        #           ou
        #
        # gitlab/jalvim/jogo-da-veia
        #
        # Lá, se tiver como, me manda a configuração que vc man-
        # dou! E pq o software é mais gostoso quando livre, faço
        # o que vc quiser com o que tiver escrito por aqui!
        #
        # Paz entre nós, guerra aos senhores!
        #                               ... VENCEREMOS!

        # Mais Ladainha e fim de jogo
        print('IMPOSSÍVEL COMO OUSA DERROTAR DEUS???!!!')
        sleep(1)
        print('no gods ...')
        sleep(1)
        print('... no masters')
        return

    # Verifica caso de empate
    elif node.score < 0:
        # Mais Ladainha ainda
        print('Você foi ', end='')
        sleep(1)
        print('HUMILHADO!')
        print('É IMPOSSÍVEL derrotar Deus...')
        return

    # Verifica caso de derrota
    else:
        # ... daí realmente foi vacilo teu mesmo kkkkkk
        print('Parabéns, vc foi capaz de empatar o jogo...')
        print('... mas nunca conseguirá ganhar de mim!!')
        print('MUAHAHAHAHAHAHA')
        return

if __name__ == '__main__':
    # Início da ladainha
    print('Olá, sou o Deus do jogo da velha, você ousa me desafiar?')
    print('Estou invocando o poder de todos os jogos possíveis!')
    print('Isso pode demorar um pouquinho...')

    # Constroi a arvora e cria vetor de folhas
    root, leaves = build_tree()
    # ... mais ladainha ...
    print('... Falta pouco! Agora só preciso digerir')
    print('Isso pode demorar mais um pouquinho...')

    # Chamei de "backprop" mas na prática é o cálculo
    # dos pesos da árvore. É nessa altura que o minimax
    # acontece na prática nesse código, pq calculei eles
    # através de uma retropropagação dos pesos. Realizei
    # minimizações e maximizações considerando a ordem
    # de jogadas com oprimeiro movimento sendo sempre do 
    # oponente.
    backprop_tree(leaves, root)

    print('Jogue comigo se for capaz!')
    # Inicia jogo
    main(root, debug_flag=False)        # Se quiser mudar pra True...
